package com.car.test.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    private CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> saveCarsAndGetAllCarsInDB(Car newCar) {
        carRepository.save(newCar);
        List<Car> carsFromDb = new ArrayList<>();
        carRepository.findAll().forEach(carsFromDb::add);
        return carsFromDb;
    }
}
