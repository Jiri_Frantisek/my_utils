package com.car.test.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class WebController {

    private CarService carService;

    @Autowired
    public WebController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/form")
    public String greetingForm(Model model) {
        model.addAttribute("car", new Car());
        return "carForm";
    }

    @PostMapping("/saveCar")
    public String saveCar(@ModelAttribute Car car, Model model) {
        List<Car> cars = carService.saveCarsAndGetAllCarsInDB(car);
        model.addAttribute("cars" ,cars);
        return "result";
    }
}
