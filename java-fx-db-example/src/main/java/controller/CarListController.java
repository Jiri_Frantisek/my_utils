package controller;

import dao.CarDao;
import entity.Car;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class CarListController implements Initializable {

    @FXML
    private TableView<Car> tableView;

    @FXML
    private TableColumn<Car, Long> idColumn;

    @FXML
    private TableColumn<Car, String> manufacturerColumn;

    @FXML
    private TableColumn<Car, String> modelColumn;

    @FXML
    private TableColumn<Car, Integer> yearColumn;

    private CarDao carDao;

    public CarListController() {
        this.carDao = new CarDao();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        manufacturerColumn.setCellValueFactory(new PropertyValueFactory<>("manufacturer"));
        modelColumn.setCellValueFactory(new PropertyValueFactory<>("model"));
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));


        List<Car> all = carDao.getAll();
        tableView.getItems().setAll(all);
    }
}
