package controller;

import dao.CarDao;
import entity.Car;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;


public class InputController {

    @FXML
    private TextField manufacturerTextField;

    @FXML
    private TextField modelTextField;

    @FXML
    private TextField registrationTextField;

    @FXML
    private TextField yearTextField;

    @FXML
    private Button resetButton;

    @FXML
    private Button saveButton;

    @FXML
    private Label infoLabel;

    private CarDao carDao;

    public InputController() {
        this.carDao = new CarDao();
    }

    public void resetTextFields(ActionEvent mouseEvent) {
        manufacturerTextField.setText("");
        modelTextField.setText("");
        registrationTextField.setText("");
        yearTextField.setText("");
    }


    public void saveData(ActionEvent mouseEvent) {
        infoLabel.setText("");
        try {
            carDao.save(new Car(modelTextField.getText(), manufacturerTextField.getText(), Integer.parseInt(yearTextField.getText())));
        } catch (NumberFormatException e) {
            infoLabel.setText("Year is not in number format!");
            yearTextField.setText("");
            return;
        }
        resetTextFields(null);
    }

    public void showNewWindow(ActionEvent actionEvent){
        FXMLLoader loader = new FXMLLoader();
        try {
            Parent rootNode = loader.load(getClass().getResourceAsStream("/fxml/list.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(rootNode);
            scene.getStylesheets().add("/styles/styles.css");
            stage.setTitle("Car list");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
