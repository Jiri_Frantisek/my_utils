package dao;

import entity.Car;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class CarDao extends GenericDao<Car> {

    public CarDao(){
        super();
    }

    public List<Car> getAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Car> list = new ArrayList<>();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("select x from Car x");
            list.addAll(query.list());
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }
}
